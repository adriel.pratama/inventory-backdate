# -*- coding: utf-8 -*-

from odoo import api, models


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.multi
    def write(self, vals):
        manual_validate_date_time = self._context.get('manual_validate_date_time', False)
        if manual_validate_date_time and 'date' in vals:
            vals['date'] = manual_validate_date_time
        return super(StockMove, self).write(vals)
