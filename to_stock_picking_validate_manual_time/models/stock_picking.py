# -*- coding: utf-8 -*-
from odoo import models, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def do_new_transfer(self):
        self.ensure_one()
        if not self._context.get('manual_validate_date_time'):
            view = self.env.ref('to_stock_picking_validate_manual_time.view_validate_datetime_manual')
            ctx = dict(self._context or {})
            ctx.update({'default_picking_id': self.id})
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.picking.validate.manual.datetime.wizard',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': ctx,
            }
        return super(StockPicking, self).do_new_transfer()

    @api.multi
    def do_transfer(self):
        res = super(StockPicking, self).do_transfer()
        manual_validate_date_time = self._context.get('manual_validate_date_time', False)
        if manual_validate_date_time:
            done_picking_ids = self.filtered(lambda x: x.state == 'done')
            done_picking_ids.write({'date_done': manual_validate_date_time})
            done_picking_ids.mapped('pack_operation_ids').write({'date': manual_validate_date_time})
            self.mapped('move_lines').filtered(lambda x: x.state == 'done').write({'date': manual_validate_date_time})
            self.sudo().mapped('move_lines').filtered(lambda x: x.state == 'done').mapped('quant_ids').write({'in_date': manual_validate_date_time})
        return res
