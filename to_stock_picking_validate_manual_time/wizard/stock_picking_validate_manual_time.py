# -*- coding: utf-8 -*-

from odoo import fields, models, _
from odoo.exceptions import UserError


class StockPickingValidateManualTime(models.TransientModel):
    _name = 'stock.picking.validate.manual.datetime.wizard'
    _description = 'Stock Picking Validate Manual Time'

    date_done = fields.Datetime(string='Date of Transfer', default=fields.Datetime.now, required=True,
                                help="The date and time at which the transfer was done.")
    picking_id = fields.Many2one('stock.picking', string="Transfer", required=True, ondelete='cascade')

    def process(self):
        self.ensure_one()
        if self.date_done > fields.Datetime.now():
            raise UserError(_("You may not be able to select a future date!"))
        return self.picking_id.with_context(manual_validate_date_time=self.date_done, force_period_date=self.date_done).do_new_transfer()
